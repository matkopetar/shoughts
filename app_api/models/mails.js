var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var mailSchema = new mongoose.Schema();
mailSchema.add({
  email: {type: String, required: true}
});

module.exports = mongoose.model('Mail', mailSchema, 'mails');
