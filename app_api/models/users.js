var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
var postSchema = new mongoose.Schema();
postSchema.add({
  _id: {type: ObjectId, "default" :"57f9790bf6e0416da08ddc1e" , required: true},
  username : {type:String, required:true},
  text: {type: String, required: true},
  //vUp: { type: Number, "default": 0, required: false},
  //vDown: { type: Number, "default": 0, required: false},
  numVotes: { type: Number, "default": 0, required: false},
  //createdOn: { type: Date, "default": Date.now, required:false},
});

var userSchema = new mongoose.Schema();
userSchema.add({
  username: {type: String, required: true},
  password: String, 
  email: String,
  avatar: {type: String},  //ovde treba saviti odredjeni tip
  creativity: { type: Number, "default": 0, required: true},
  vUp: { type: Number, "default": 0, required: true},
  vDown: { type: Number, "default": 0, required: true},
  oneWordDescription: String,
  following: [ObjectId],
  dateJoin: {type: Date, "default": Date.now},
  posts: [postSchema]
});

module.exports = mongoose.model('User', userSchema, 'users');
