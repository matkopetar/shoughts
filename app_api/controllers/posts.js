var mongoose = require('mongoose');
var User = require('../models/users');

var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
}
var randUser = ['Einstein', 'Tesla', 'Jefferson', 'Curie', 'Oppenheimer', 'Da Vinci', 'Galilei', 'Velebit', 'Newton','Lennox', 'Nash', 'Planck', 'De Broglie', 'Heisenberg', 'Schrodinger', 'Turing', 'Hawking', 'Darwin', 'Faraday', 'Maxwell', 'Pupin', 'Wise turtle']
var randomUser = randUser[Math.floor(Math.random() * randUser.length)];
var addPost = function(req, res, user) {

    user.posts.unshift({
      username: randomUser,
      text: req.body.text
    });
    user.save(function(err) {
        if(err) {
            sendJsonResponse(res, 404, err);
            return;
        }
        else {
            sendJsonResponse(res, 200, user.posts);
            console.log("New shought added!");
            
        }
    });
  }

module.exports.postsReadAll = function(req, res) {
  User.find({})
  .exec(function(err, users) {
    if(!users) {
      sendJsonResponse(res, 404, {"message":"users not found"});
      return;
    }
    else if (err) {
      sendJsonResponse(res, 404, err);
      return;
    }
    else {
      var postMap = [];

      users.forEach(function(user) {
        user.posts.forEach(function(post) {
          postMap.push(post);
        });
      });

      sendJsonResponse(res, 200, postMap);
    }
  })  
}

module.exports.postsReadAllFromOneUser = function(req, res) {
  if(req.params && req.params.userid) {
    User.findById(req.params.userid)
    .exec(function(err, user) {
      if(!user) {
        sendJsonResponse(res, 404, {"message":"user not found"});
        return;
      }
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      else { 
        sendJsonResponse(res, 200, user.posts);
      }
    })
  }
}

module.exports.postsReadAllFromAllFollowers = function(req, res) {
  if(req.params && req.params.userid) {
    User.findById(req.params.userid)
    .exec(function(err, user) {
      if(!user) {
        sendJsonResponse(res, 404, {"message":"user not found"});
        return;
      }
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      else {
        var postMap = [];

        var getPosts = function(followingId) {
          var posts = [];
          User.findById(followingId)
          .exec(function(err, user2) {
            user2.posts.forEach(function(post) {
              posts.push(post);
            });
            return posts;
          });
        }

        user.following.forEach(function(followingId) {
          postMap.push(getPosts(followingId));
        });

      sendJsonResponse(res, 200, postMap);
      }
    }) 
  }
}///////////////////////

module.exports.postsReadOne = function(req, res) {
  if(req.params && req.params.userid && req.params.postid) {
    User.findById(req.params.userid)
    .exec(function(err, user) {
      if(!user) {
        sendJsonResponse(res, 404, {"message":"user not found"});
        return;
      }
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      else {
        thisPost = user.posts.id(req.params.postid);
        sendJsonResponse(res, 200, thisPost);
      }
    })
  }
}

module.exports.postsCreateOne = function(req, res) {
  if(req.params && req.params.userid) {
    User.findById(req.params.userid)
    .exec(function(err, user) {
      if(!user) {
        sendJsonResponse(res, 404, {"message":"user not found"});
        return;
      }
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      } 
      else {
        addPost(req, res, user);
      }
    })
  }
  else {
    sendJsonResponse(res, 404, {"message": "No user name in request"});
  }  
}



module.exports.postsDeleteOne = function(req, res) {
  if(req.params && req.params.userid && req.params.postid) {
    User.findById(req.params.userid)
    .exec(function(err, user) {
      if(!user) {
        sendJsonResponse(res, 404, {"message":"user not found"});
        return;
      }
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      else {
        thisPost = user.posts.id(req.params.postid);
        thisPost.pull();
      }
    })
  } 
}

