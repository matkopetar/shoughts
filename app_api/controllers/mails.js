var mongoose = require('mongoose');
var Mail = require('../models/mails');

var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
}

module.exports.mailCreateOne = function(req, res) {

  if(req.body) {
    var post = new Mail({
      email: req.body.mail
    });
    post.save(function (err) {
      if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      else {
        sendJsonResponse(res, 200, post);
      }
    });
  }
  else {
    sendJsonResponse(res, 404, {"message":"body doesn't exist"})
  }
}