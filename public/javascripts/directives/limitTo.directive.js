var limitTo = function($filter) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
                if ($filter('izbrojiReci')(this.value) == limit) {
                    if(e.keyCode == 32) {
                        e.preventDefault();
                    }
                }
            });
        }
    }
}